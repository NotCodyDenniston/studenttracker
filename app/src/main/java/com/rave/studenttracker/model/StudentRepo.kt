package com.rave.studenttracker.model

import com.rave.studenttracker.model.entity.Student
import com.rave.studenttracker.model.mapper.student.StudentMapper
import com.rave.studenttracker.model.remote.StudentApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

/**
 * Student repo.
 *
 * @property studentApi
 * @property studentMapper
 * @constructor Create empty Student repo
 */
class StudentRepo(
    private val studentApi: StudentApi,
    private val studentMapper: StudentMapper
) {
    /**
     * Get student list returns a list of mapped students on the IO thread.
     *
     * @return
     */

    suspend fun getStudentList(): List<Student> {
        return withContext(Dispatchers.IO) {
            studentApi.fetchStudentList().map { studentMapper.invoke(it) }
        }
    }
}
