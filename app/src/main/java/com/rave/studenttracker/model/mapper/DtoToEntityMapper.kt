package com.rave.studenttracker.model.mapper

/**
 * Dto to entity mapper, maps the received data to just the data we want.
 *
 * @param DTO this is the received student
 * @param ENTITY this is the student we build
 * @constructor Create empty Dto to entity mapper
 */

interface DtoToEntityMapper<in DTO, out ENTITY> {
    /**
     * Invoke.
     *
     * @param dto
     * @return
     */
    operator fun invoke(dto: DTO): ENTITY
}
