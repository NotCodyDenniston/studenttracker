package com.rave.studenttracker.model.entity

/**
 * Student class the we use to build from the studentDTO data.
 *
 * @property avatar
 * @property email
 * @property firstName
 * @property id
 * @property lastName
 * @property university
 * @constructor Create empty Student
 */

data class Student(
    val avatar: String,
    val email: String,
    val firstName: String,
    val id: Int,
    val lastName: String,
    val university: String
)
