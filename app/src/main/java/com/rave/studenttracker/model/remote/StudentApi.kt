package com.rave.studenttracker.model.remote

import com.rave.studenttracker.model.dto.StudentDTO

/**
 * Student api is the api that interacts with the student data.
 *
 * @constructor Create empty Student api
 */

interface StudentApi {

    /**
     * Fetch student list returns the list of studentDTOs.
     *
     * @return
     */

    suspend fun fetchStudentList(): List<StudentDTO>
}
