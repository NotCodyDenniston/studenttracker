package com.rave.studenttracker.model.remote

import com.rave.studenttracker.model.dto.StudentDTO
import kotlinx.coroutines.delay
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json

internal class StudentApiImpl(fakeJsonString: String) : StudentApi {

    private val fakeStudents: List<StudentDTO> by lazy { Json.decodeFromString(fakeJsonString) }
    private val delayPage: Long = 2000

    override suspend fun fetchStudentList(): List<StudentDTO> {
        delay(timeMillis = delayPage)
        return fakeStudents
    }
}
