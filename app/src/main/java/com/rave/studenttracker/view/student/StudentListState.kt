package com.rave.studenttracker.view.student

import com.rave.studenttracker.model.entity.Student

/**
 * Student list state is the state of the students data.
 *
 * @property isLoading is a property that is true if the students haven't loaded yet
 * @property students is the list that the students will be passed to
 * @constructor Create empty Student list state
 */

data class StudentListState(
    val isLoading: Boolean = false,
    val students: List<Student> = emptyList()
)
