package com.rave.studenttracker.view.student

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.rave.studenttracker.model.entity.Student

@Composable
fun StudentListScreen(students: List<Student>) {
    LazyColumn {
        items(
            items = students,
            key = { student: Student -> student.id }
        ) { student: Student ->
            StudentCard(student = student)
        }
    }
}

@Composable
fun StudentCard(student: Student) {
    Column(Modifier.padding(10.dp)) {
        AsyncImage(model = student.avatar, contentDescription = null, modifier = Modifier.width(50.dp).height(50.dp))
        Text(text = "${student.firstName} ${student.lastName}")
        Text(text = "Email: ${student.email}")
        Text(text = "University: ${student.university}")
    }
}
